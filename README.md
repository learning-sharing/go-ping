### 编译工具
go build -o 生成文件名 main.go

### golang开发批量ping ip工具
>1. 用户可以指定ip范围, 
>2. 指定并发数量. 
>3. 用于查询本地在线设备数量, ip使用数量
>4. 可用于window, linux, mac系统

#### 使用说明
ping.exe --help
```bash
Usage of ping.exe:
  -ip string
        ip地址: 192.168.1.1-192.168.1.255 (default "192.168.1.1")
  -size int
        并发数量: 10(默认) (default 10)
```



#### 访问结果示例
* ping.exe -ip 192.168.115.80-192.168.115.85
```bash
ping 192.168.115.83 结果为: true
ping 192.168.115.85 结果为: true
ping 192.168.115.82 结果为: true
ping 192.168.115.84 结果为: false
ping 192.168.115.81 结果为: false
ping 192.168.115.80 结果为: false
ip总数:  6
ip可访问:  3
ip不可访问:  3
```

