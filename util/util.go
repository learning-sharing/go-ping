package util

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

// 端口遍历
func PortParse(port string) []int {
	var resultPorts []int
	ports := strings.Split(port, "-")
	if len(ports) < 1 {
		return resultPorts
	}
	portOne, _ := strconv.Atoi(ports[0])
	if len(ports) == 1 {
		resultPorts = append(resultPorts, portOne)
		return resultPorts
	}
	portTwo, _ := strconv.Atoi(ports[1])
	if portOne > portTwo {
		tmp := portOne
		portOne = portTwo
		portTwo = tmp
	}
	for i := portOne; i <= portTwo; i++ {
		resultPorts = append(resultPorts, i)
	}
	return resultPorts
}

// ip地址解析
func IpParse(ip string) []string {
	var resultIps []string
	ips := strings.Split(ip, "-")
	if len(ips) < 0 {
		return resultIps
	}
	// 校验ip地址
	for _, p := range ips {
		if !Ipv4Valid(p) {
			return resultIps
		}
	}
	if len(ips) == 1 {
		ipOne := strings.Split(ips[0], ".")
		ipOneLast, _ := strconv.Atoi(ipOne[3])
		for i := ipOneLast; i <= 255; i++ {
			rip := fmt.Sprintf("%s.%s.%s.%d", ipOne[0], ipOne[1], ipOne[2], i)
			resultIps = append(resultIps, rip)
		}
	}
	if len(ips) == 2 {
		ipOne := strings.Split(ips[0], ".")
		ipTwo := strings.Split(ips[1], ".")
		ipOne1, _ := strconv.Atoi(ipOne[0])
		ipTwo1, _ := strconv.Atoi(ipTwo[0])
		ipOne2, _ := strconv.Atoi(ipOne[1])
		ipTwo2, _ := strconv.Atoi(ipTwo[1])
		ipOne3, _ := strconv.Atoi(ipOne[2])
		ipTwo3, _ := strconv.Atoi(ipTwo[2])
		ipOne4, _ := strconv.Atoi(ipOne[3])
		ipTwo4, _ := strconv.Atoi(ipTwo[3])
		if ipOne1 < ipTwo1 {
			for ipOne1 <= ipTwo1 {
				i2s := 1
				i2e := 255
				if ipOne1 < ipTwo1 {
					i2s = ipOne2
				}
				if ipOne1 == ipTwo1 {
					i2s = ipOne2
					i2e = ipTwo2
				}
				for i2 := i2s; i2 <= i2e; i2++ {
					i3s := 1
					i3e := 255
					if i2 == ipTwo2 {
						i3s = ipOne3
						i3e = ipTwo3
					}
					for i3 := i3s; i3 <= i3e; i3++ {
						i4s := 1
						i4e := 255
						if i3 == ipTwo3 {
							i4s = ipOne4
							i4e = ipTwo4
						}
						for i4 := i4s; i4 <= i4e; i4++ {
							rip := fmt.Sprintf("%d.%d.%d.%d", ipOne1, i2, i3, i4)
							resultIps = append(resultIps, rip)
						}
					}
				}
				ipOne1++
			}
			return resultIps
		}
		if ipOne2 < ipTwo2 {
			for i2 := ipOne2; i2 <= ipTwo2; i2++ {
				i3s := 1
				i3e := 255
				if i2 < ipTwo2 {
					i3s = ipOne3
				}
				if i2 == ipTwo2 {
					i3s = ipOne3
					i3e = ipTwo3
				}
				for i3 := i3s; i3 <= i3e; i3++ {
					i4s := 1
					i4e := 255
					if i3 < ipTwo3 {
						i4s = ipOne4
					}
					if i3 == ipTwo3 {
						i4s = ipOne4
						i4e = ipTwo4
					}
					for i4 := i4s; i4 <= i4e; i4++ {
						rip := fmt.Sprintf("%d.%d.%d.%d", ipOne1, i2, i3, i4)
						resultIps = append(resultIps, rip)
					}
				}
			}
			return resultIps
		}

		if ipOne3 < ipTwo3 {
			for i3 := ipOne3; i3 <= ipTwo3; i3++ {
				i4s := 1
				i4e := 255
				if i3 < ipTwo3 {
					i4s = ipOne4
				}
				if i3 == ipTwo3 {
					i4s = ipOne4
					i4e = ipTwo4
				}
				for i4 := i4s; i4 <= i4e; i4++ {
					rip := fmt.Sprintf("%d.%d.%d.%d", ipOne1, ipOne2, i3, i4)
					resultIps = append(resultIps, rip)
				}
			}
			return resultIps
		}

		if ipOne4 < ipTwo4 {
			for i4 := ipOne4; i4 <= ipTwo4; i4++ {
				rip := fmt.Sprintf("%d.%d.%d.%d", ipOne1, ipOne2, ipOne3, i4)
				resultIps = append(resultIps, rip)
			}
			return resultIps
		}
	}
	return resultIps
}

// ipv4验证
func Ipv4Valid(ip string) bool {
	ipReg := `^((0|[1-9]\d?|1\d\d|2[0-4]\d|25[0-5])\.){3}(0|[1-9]\d?|1\d\d|2[0-4]\d|25[0-5])$`
	match, _ := regexp.MatchString(ipReg, ip)
	if match {
		return true
	}
	return false
}
