package main

import (
	"cntech/study/exec/ping/lib"
	"cntech/study/exec/ping/util"
	"flag"
	"fmt"
)

var pool *lib.Pool

var total, success, fail int64

func main() {
	ip := flag.String("ip", "192.168.1.1", "ip地址: 192.168.1.1-192.168.1.255")
	//port := flag.String("port", "80", "ip地址端口: 80(默认),80-90")
	queueSzie := flag.Int("size", 10, "并发数量: 10(默认)")
	//解析输入的参数
	flag.Parse()
	ips := util.IpParse(*ip)
	//fmt.Println("输出的参数port的值是:", *port)
	//ports := util.PortParse(*port)
	PingIps(ips, *queueSzie)
}

// ping ip地址
func PingIps(ips []string, queueNum int) {
	if len(ips) < 1 {
		return
	}
	if queueNum < 1 {
		queueNum = 10
	}
	pool = lib.NewPool(queueNum)
	for _, ip := range ips {
		pool.Add(1)
		go ping(ip)
	}
	pool.Wait()
	fmt.Println("ip总数: ", total)
	fmt.Println("ip可访问: ", success)
	fmt.Println("ip不可访问: ", fail)
}

func ping(ip string) {
	total++
	b := lib.Ping(ip)
	if b == true {
		success++
		fmt.Printf("ping %s 结果为: %v\n", ip, b)
	} else {
		fail++
		fmt.Printf("ping %s 结果为: %v\n", ip, b)
	}
	pool.Done()
}
